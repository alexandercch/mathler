import { createSlice } from '@reduxjs/toolkit';
import type { RootState } from './store';
import { COLS } from '../constants';

export interface EquationState {
  value: number;
  equation: string;
}

const initialState: EquationState = {
  value: 0,
  equation: '',
};

const getRandomInt = (top: number) => Math.floor(Math.random() * top);

export const equationSlice = createSlice({
  name: 'equation',
  initialState,
  reducers: {
    generate: (state) => {
      // TODO find better algorithm to generate random equation
      const operands = '+/-*-*+/';
      let equationCandidate = `${getRandomInt(10) * getRandomInt(10)}`;
      while (equationCandidate.length < COLS) {
        if (equationCandidate.length + 1 === COLS) {
          // final seat, pick number
          equationCandidate += `${getRandomInt(10)}`;
        } else if (operands.includes(equationCandidate.slice(-1))) {
          equationCandidate += `${getRandomInt(10) * getRandomInt(10) + 1}`;
        } else {
          equationCandidate += operands[getRandomInt(operands.length)];
        }
      }

      // eslint-disable-next-line no-console
      console.info('equationCandidate', equationCandidate);
      state.equation = equationCandidate;
      // eslint-disable-next-line no-eval
      state.value = eval(`(${equationCandidate})`);
    },
  },
});

export const { generate } = equationSlice.actions;

export const selectEquation = (state: RootState) => state.equation;

export default equationSlice.reducer;
