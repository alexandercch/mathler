import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import App from './App';
import { store } from './redux/store';

describe('renders ui', () => {
  render(
    <Provider store={store}>
      <App />
    </Provider>,
  );
  it('should display basic elements', () => {
    const title = screen.getByText(/Mathler/i);
    expect(title).toBeInTheDocument();
    const subTitle = screen.getByText(/Find the hidden calculation that equals/i);
    expect(subTitle).toBeInTheDocument();
    const enter = screen.getByText(/enter/i);
    expect(enter).toBeInTheDocument();
  });
});
