import React from 'react';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import {
  typeIn, typeDelete, typeEnter, selectBoard,
} from '../redux/boardSlice';
import { Block } from './styles/Block.styled';
import { KeyboardWrapper } from './styles/KeyboardWrapper.styled';
import { selectEquation } from '../redux/equationSlice';
import { keyboard, COLS } from '../constants';

export default function Keyboard() {
  const dispatch = useAppDispatch();
  const { matrix, row } = useAppSelector(selectBoard);
  const { equation, value } = useAppSelector(selectEquation);

  const computeState = (key: string) => {
    let state = 'key';
    for (let r = 0; r < row; r += 1) {
      for (let c = 0; c < COLS; c += 1) {
        if (
          matrix[r][c]
          && equation[c] === matrix[r][c]
          && equation[c] === key
        ) {
          return 'correct';
        }
        if (equation.includes(matrix[r][c]) && matrix[r][c] === key) {
          state = 'exist';
        }
      }
    }
    return state;
  };

  const keyClick = (key: string) => {
    if (key === 'Enter') {
      dispatch(typeEnter({ value, equation }));
    } else if (key === 'Delete') {
      dispatch(typeDelete());
    } else {
      dispatch(typeIn(key));
    }
  };

  return (
    <>
      {keyboard.map((keyRow: string[]) => (
        <KeyboardWrapper key={keyRow[0]}>
          {keyRow.map((key: string) => (
            <Block key={key} type="key" state={computeState(key)} onClick={() => keyClick(key)}>
              {key}
            </Block>
          ))}
        </KeyboardWrapper>
      ))}
    </>
  );
}
