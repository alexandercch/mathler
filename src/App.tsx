/* eslint-disable jsx-a11y/no-noninteractive-tabindex */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useEffect, useRef } from 'react';
import './App.css';
import Header from './components/Header';
import Board from './components/Board';
import Keyboard from './components/Keyboard';

import { typeIn, typeDelete, typeEnter } from './redux/boardSlice';
import { useAppDispatch, useAppSelector } from './redux/hooks';
import { generate, selectEquation } from './redux/equationSlice';

import { keyboard } from './constants';
import Footer from './components/Footer';

function App() {
  const dispatch = useAppDispatch();
  const { equation, value } = useAppSelector(selectEquation);
  const divRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    dispatch(generate());
  }, []);

  useEffect(() => {
    if (divRef.current) {
      divRef.current.focus();
    }
  }, [divRef]);

  const handleTyping = (e: React.KeyboardEvent) => {
    if (e.key === 'Enter') {
      dispatch(typeEnter({ value, equation }));
    } else if (e.key === 'Backspace') {
      dispatch(typeDelete());
    } else {
      // sent typeIn only valid keyboard set
      keyboard.forEach((row: string[]) =>
        // eslint-disable-next-line implicit-arrow-linebreak
        row.forEach((key: string) => key === e.key && dispatch(typeIn(e.key))));
    }
  };

  return (
    <div className="App" tabIndex={0} ref={divRef} onKeyUp={handleTyping}>
      <Header />
      <Board />
      <Keyboard />
      <Footer />
    </div>
  );
}

export default App;
