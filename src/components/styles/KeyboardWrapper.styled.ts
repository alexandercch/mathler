/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';

export const KeyboardWrapper = styled.div`
  margin: auto;
  max-width: 400px;
  display: flex;
  justify-content: center;
`;
