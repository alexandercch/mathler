/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';

export const BoardWrapper = styled.div`
  margin: 5px auto;
  max-width: 400px;
  margin-bottom: 15px;
  & > div {
    display: flex;
    justify-content: center;
  }
`;
