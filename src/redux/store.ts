import { configureStore } from '@reduxjs/toolkit';
import boardReducer from './boardSlice';
import equationReducer from './equationSlice';

export const store = configureStore({
  reducer: {
    board: boardReducer,
    equation: equationReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
