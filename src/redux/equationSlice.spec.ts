import equationReducer, { generate } from './equationSlice';

describe('equation reducer', () => {
  it('should generate a valid equation', () => {
    const current = equationReducer(undefined, generate());
    expect(current.equation.length).toEqual(6);
    // eslint-disable-next-line no-eval
    expect(eval(`(${current.equation})`)).toEqual(current.value);
  });
});
