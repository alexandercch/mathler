import React from 'react';
import { useAppSelector } from '../redux/hooks';
import { selectEquation } from '../redux/equationSlice';

export default function Header() {
  const { value } = useAppSelector(selectEquation);
  return (
    <>
      <h1>MATHLER</h1>
      <h2>
        Find the hidden calculation that equals
        {` ${value}`}
      </h2>
    </>
  );
}
