import React, { useEffect, useState } from 'react';
import { useAppSelector, useAppDispatch } from '../redux/hooks';
import { selectBoard, restart } from '../redux/boardSlice';
import { generate } from '../redux/equationSlice';
import { RESTART_COUNT } from '../constants';

interface Props {
  update: ()=> void;
  finish: ()=> void;
  interval?: number;
}

function IntervalCaller({ update, finish, interval = 1000 }: Props) {
  useEffect(() => {
    const id = setInterval(update, interval);
    return () => {
      clearInterval(id);
      finish();
    };
  }, []);
  return null;
}

export default function Footer() {
  const dispatch = useAppDispatch();

  const { gameState } = useAppSelector(selectBoard);
  const [countDown, setCountDown] = useState(-1);

  const restartGame = () => {
    dispatch(generate());
    dispatch(restart());
    setCountDown(-1);
  };

  useEffect(() => {
    if (gameState !== 'playing') {
      setCountDown(RESTART_COUNT);
    }
  }, [gameState]);

  return countDown !== -1 ? (
    <>
      <h4>
        Game will automatically restart in
        {countDown}
      </h4>
      <IntervalCaller
        update={() => setCountDown((prev) => prev - 1)}
        finish={restartGame}
      />
      <button onClick={() => setCountDown(-1)} type="button">
        Play again now!
      </button>
    </>
  ) : null;
}
