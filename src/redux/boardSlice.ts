import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import type { RootState } from './store';
import { ROWS, COLS } from '../constants';
import { evalEquation } from '../utils';

export interface BoardState {
  matrix: string[];
  row: number;
  col: number;
  gameState: string;
}

const initialState: BoardState = {
  matrix: Array(ROWS).fill(''),
  row: 0,
  col: 0,
  gameState: 'playing', // playing | win | gameOver
};

export const boardSlice = createSlice({
  name: 'board',
  initialState,
  reducers: {
    typeIn: (state, action: PayloadAction<string>) => {
      const { row, col } = state;
      if (row < ROWS && col < COLS) {
        state.matrix[row] += action.payload;
        state.col += 1;
      }
    },
    typeDelete: (state) => {
      const { row, col } = state;
      if (col > 0) {
        state.matrix[row] = state.matrix[row].slice(0, -1);
        state.col -= 1;
      }
    },
    typeEnter: (
      state,
      action: PayloadAction<{ value: number; equation: string }>,
    ) => {
      if (state.col === COLS && state.row < ROWS) {
        const { value, equation } = action.payload;
        if (evalEquation(value, state.matrix[state.row])) {
          if (equation === state.matrix[state.row]) {
            state.gameState = 'win';
          } else if (state.row + 1 === ROWS) {
            // will run out of rows
            state.gameState = 'gameOver';
          }
          state.row += 1;
          state.col = 0;
        } else {
          toast.info('Equation result is not the same as the value');
        }
      }
    },
    restart: () => initialState,
  },
});

export const {
  typeIn, typeDelete, typeEnter, restart,
} = boardSlice.actions;

export const selectBoard = (state: RootState) => state.board;

export default boardSlice.reducer;
