import React, { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { BoardWrapper } from './styles/BoardWrapper.styled';
import { useAppSelector } from '../redux/hooks';
import { selectBoard } from '../redux/boardSlice';
import { selectEquation } from '../redux/equationSlice';
import { Block } from './styles/Block.styled';
import { COLS } from '../constants';

export default function Board() {
  const { matrix, row, gameState } = useAppSelector(selectBoard);
  const { equation } = useAppSelector(selectEquation);

  useEffect(() => {
    // eslint-disable-next-line no-console
    console.info('gameState', gameState);
    if (gameState === 'win') {
      toast.success('You win!!');
    }
    if (gameState === 'gameOver') {
      toast.warning(`You runout of tries, the equation was: ${equation}`);
    }
  }, [gameState]);

  const computeState = (r: number, c: number) => {
    if (r < row) {
      if (equation.includes(matrix[r][c])) {
        return matrix[r][c] === equation[c] ? 'correct' : 'exist';
      }
      return 'incorrect';
    }
    if (r === row) {
      return matrix[r][c] ? 'edit' : 'start';
    }
    return 'start';
  };

  return (
    <BoardWrapper>
      {matrix.map((line: string, index: number) => {
        const blocks: React.ReactNode[] = [];
        for (let c = 0; c < COLS; c += 1) {
          blocks.push(
            <Block key={Math.random()} type="block" state={computeState(index, c)}>{line[c] || ''}</Block>,
          );
        }
        return <div key={Math.random()}>{blocks}</div>;
      })}
      <ToastContainer position="top-center" />
    </BoardWrapper>
  );
}
