// eslint-disable-next-line import/prefer-default-export
export const evalEquation = (value: number, equationCandidate: string) => {
  try {
    // eslint-disable-next-line no-eval
    const valueCandidate = eval(`(${equationCandidate})`);
    return valueCandidate === value;
  } catch (error) {
    return false;
  }
};
