/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';

type BlockProps = {
  state: string;
  type: string; // block | key
};

export const Block = styled.div<BlockProps>`
  border-radius: 4px;
  box-sizing: border-box;
  font-size: 17px;
  ${({ type }) => type === 'block'
    && `
      min-width: 56px;
      margin: 2px;
      max-width: 100px;
      height: 40px;
      line-height: 40px;
  `};
  ${({ type }) => type === 'key'
    && `
      min-width: 40px;
      margin: 2px;
      max-width: 100px;
      height: 50px;
      line-height: 50px;
      font-weight: 600;
      padding: 0 5px;
      cursor: pointer;
  `};
  ${({ state }) => state === 'start'
    && `
        border: 1px solid #e2e8f0;
        background-color: white;
    `};
  ${({ state }) => state === 'edit'
    && `
        border: 2px solid #000;
        background-color: white;
        color: #000;
    `};
  ${({ state }) => state === 'incorrect'
    && `
        background-color: #94a3b8;
        color: #fff;
    `};
  ${({ state }) => state === 'exist'
    && `
        background-color: #eab308;
        color: #fff;
    `};
  ${({ state }) => state === 'correct'
    && `
        background-color: #22c55e;
        color: #fff;
    `};
  ${({ state }) => state === 'key'
    && `
        background-color: #e2e8f0;
        color: #000;
    `};
`;
