export const keyboard = [
  ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
  ['Enter', '+', '-', '*', '/', 'Delete'],
];

export const ROWS = 6;
export const COLS = 6;

export const RESTART_COUNT = 120;
