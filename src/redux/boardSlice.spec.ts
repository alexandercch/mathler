import boardReducer, {
  BoardState,
  typeIn,
  typeDelete,
  typeEnter,
} from './boardSlice';

describe('board reducer', () => {
  const initialState: BoardState = {
    matrix: ['12+2+1', '24', '', '', '', ''],
    row: 1,
    col: 2,
    gameState: 'playing',
  };

  it('should allow type new character', () => {
    const current = boardReducer(initialState, typeIn('/'));
    expect(current.matrix[1]).toEqual('24/');
  });

  it('should allow delete a character', () => {
    const current = boardReducer(initialState, typeDelete());
    expect(current.matrix[1]).toEqual('2');
  });

  it('should allow type enter and move to new row', () => {
    let current = boardReducer(initialState, typeIn('-'));
    current = boardReducer(current, typeIn('3'));
    current = boardReducer(current, typeIn('-'));
    current = boardReducer(current, typeIn('9'));
    expect(current.matrix[1]).toEqual('24-3-9');
    current = boardReducer(
      current,
      typeEnter({ value: 12, equation: '1+10-1' }),
    );
    expect(current.row).toEqual(2);
    expect(current.col).toEqual(0);
  });

  it('show set state to win', () => {
    let current = boardReducer(initialState, typeIn('-'));
    current = boardReducer(current, typeIn('1'));
    current = boardReducer(current, typeIn('-'));
    current = boardReducer(current, typeIn('9'));
    current = boardReducer(
      current,
      typeEnter({ value: 14, equation: '24-1-9' }),
    );
    expect(current.row).toEqual(2);
    expect(current.col).toEqual(0);
    expect(current.gameState).toEqual('win');
  });

  it('show set state to gameOver', () => {
    const input:string[] = ['1-9', '20-3-3', '27-7-6', '5+10-1', '5+11-2'];
    let current = boardReducer(initialState, typeIn('-'));
    for (let i = 0; i < input.length; i += 1) {
      for (let j = 0; j < input[i].length; j += 1) {
        current = boardReducer(current, typeIn(input[i][j]));
      }
      current = boardReducer(
        current,
        typeEnter({ value: 14, equation: '24-2-8' }),
      );
      expect(current.gameState).toEqual(i < 4 ? 'playing' : 'gameOver');
    }
  });
});
